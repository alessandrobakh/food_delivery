<?php

namespace App\Providers;

use App\Models\Card;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\Institution;
use App\Models\InstitutionCategory;
use App\Policies\CardPolicy;
use App\Policies\DishCategoryPolicy;
use App\Policies\DishPolicy;
use App\Policies\InstitutionCategoryPolicy;
use App\Policies\InstitutionPolicy;
use App\Policies\PagesPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        DishCategory::class => DishCategoryPolicy::class,
        Dish::class => DishPolicy::class,
        InstitutionCategory::class => InstitutionCategoryPolicy::class,
        Institution::class => InstitutionPolicy::class,
        Card::class => CardPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('viewAny', [PagesPolicy::class, 'viewAny']);
    }
}
