<?php

namespace App\Policies;

use App\Models\DishCategory;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DishCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin;
    }
}
