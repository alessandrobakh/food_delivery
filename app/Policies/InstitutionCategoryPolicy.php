<?php

namespace App\Policies;

use App\Models\InstitutionCategory;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstitutionCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin;
    }
}
