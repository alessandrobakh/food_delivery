<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Dish extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'picture', 'price', 'desc', 'institution_id', 'dish_category_id'];

    /**
     * @return BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo(Institution::class);
    }

    /**
     * @return BelongsTo
     */
    public function dishCategory()
    {
        return $this->belongsTo(DishCategory::class);
    }
}
