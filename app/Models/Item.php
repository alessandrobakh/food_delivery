<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Item extends Model
{
    use HasFactory;

    protected $fillable = ['card_id', 'dish_id', 'price', 'dish_name', 'qty'];

    /**
     * @return BelongsTo
     */
    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    /**
     * @return BelongsTo
     */
    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }
}
