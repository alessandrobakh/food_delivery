<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Institution extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'picture', 'desc', 'institution_category_id'];

    /**
     * @return HasMany
     */
    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }

    /**
     * @return BelongsTo
     */
    public function institutionCategory()
    {
        return $this->belongsTo(InstitutionCategory::class);
    }

    /**
     * @return HasMany
     */
    public function cards()
    {
        return $this->hasMany(Card::class);
    }
}
