<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InstitutionRequest;
use App\Models\Institution;
use App\Models\InstitutionCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InstitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny');
        $institutions = Institution::all();
        return view('admin.institutions.index', compact('institutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('viewAny');
        $instCategories = InstitutionCategory::all();
        return view('admin.institutions.create', compact('instCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InstitutionRequest $request
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(InstitutionRequest $request)
    {
        $this->authorize('viewAny');
        $institution = new Institution($request->all());
        if ($request->hasFile('picture')){
            $institution->picture = $request->file('picture')->store('pictures', 'public');
        }
        $institution->save();
        return redirect()->route('admin.institutions.index')->with('status', 'institution created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Institution $institution
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Institution $institution)
    {
        $this->authorize('viewAny');
        return view('admin.institutions.show', compact('institution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Institution $institution
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Institution $institution)
    {
        $this->authorize('viewAny');
        $instCategories = InstitutionCategory::all();
        return view('admin.institutions.edit', compact('institution', 'instCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InstitutionRequest $request
     * @param \App\Models\Institution $institution
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(InstitutionRequest $request, Institution $institution)
    {
        $this->authorize('viewAny');
        $data = $request->all();
        if ($request->hasFile('picture')){
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $institution->update($data);
        return redirect()->route('admin.institutions.index')->with('status', 'institution updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Institution $institution
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Institution $institution)
    {
        $this->authorize('viewAny');
        $institution->delete();
        return redirect()->route('admin.institutions.index')->with('status', 'institution deleted!');
    }
}
