<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DishCategoryRequest;
use App\Models\DishCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DishCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny');
        $dishCategories = DishCategory::all();
        return view('admin.dishCategories.index', compact('dishCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('viewAny');
        return view('admin.dishCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DishCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(DishCategoryRequest $request)
    {
        $this->authorize('viewAny');
        $dishCategory = new DishCategory($request->all());
        $dishCategory->save();
        return redirect()->route('admin.dishCategories.index')->with('status', 'dish category created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\DishCategory $dishCategory
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(DishCategory $dishCategory)
    {
        $this->authorize('viewAny');
        return view('admin.dishCategories.show', compact('dishCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\DishCategory $dishCategory
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(DishCategory $dishCategory)
    {
        $this->authorize('viewAny');
        return view('admin.dishCategories.edit', compact('dishCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DishCategoryRequest $request
     * @param \App\Models\DishCategory $dishCategory
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(DishCategoryRequest $request, DishCategory $dishCategory)
    {
        $this->authorize('viewAny');
        $dishCategory->update($request->all());
        return redirect()->route('admin.dishCategories.index')->with('status', 'dish category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\DishCategory $dishCategory
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(DishCategory $dishCategory)
    {
        $this->authorize('viewAny');
        $dishCategory->delete();
        return redirect()->route('admin.dishCategories.index')->with('status', 'dish category deleted!');
    }
}
