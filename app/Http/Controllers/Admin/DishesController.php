<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DishRequest;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\Institution;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny');
        $dishes = Dish::all();
        return view('admin.dishes.index', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('viewAny');
        $dishCategories = DishCategory::all();
        $institutions = Institution::all();
        return view('admin.dishes.create', compact('dishCategories', 'institutions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DishRequest $request
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(DishRequest $request)
    {
        $this->authorize('viewAny');
        $dish = new Dish($request->all());
        if ($request->hasFile('picture')){
            $dish->picture = $request->file('picture')->store('pictures', 'public');
        }
        $dish->save();
        return redirect()->route('admin.dishes.index')->with('status', 'dish created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Dish $dish
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Dish $dish)
    {
        $this->authorize('viewAny');
        return view('admin.dishes.show', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Dish $dish
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Dish $dish)
    {
        $this->authorize('viewAny');
        $dishCategories = DishCategory::all();
        $institutions = Institution::all();
        return view('admin.dishes.edit', compact('dish','dishCategories', 'institutions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DishRequest $request
     * @param \App\Models\Dish $dish
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(DishRequest $request, Dish $dish)
    {
        $this->authorize('viewAny');
        $data = $request->all();
        if ($request->hasFile('picture')){
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $dish->update($data);
        return redirect()->route('admin.dishes.index')->with('status', 'dish updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Dish $dish
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Dish $dish)
    {
        $this->authorize('viewAny');
        $dish->delete();
        return redirect()->route('admin.dishes.index')->with('status', 'dish deleted!');
    }
}
