<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InstitutionCategoryRequest;
use App\Models\InstitutionCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InstitutionCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny');
        $instCategories = InstitutionCategory::all();
        return view('admin.institutionCategories.index', compact('instCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('viewAny');
        return view('admin.institutionCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InstitutionCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(InstitutionCategoryRequest $request)
    {
        $this->authorize('viewAny');
        $instCategory = new InstitutionCategory($request->all());
        $instCategory->save();
        return redirect()->route('admin.institutionCategories.index')->with('status', 'institution category created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\InstitutionCategory $institutionCategory
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(InstitutionCategory $institutionCategory)
    {
        $this->authorize('viewAny');
        return view('admin.institutionCategories.show', compact('institutionCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\InstitutionCategory $institutionCategory
     * @return Factory|View|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(InstitutionCategory $institutionCategory)
    {
        $this->authorize('viewAny');
        return view('admin.institutionCategories.edit', compact('institutionCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InstitutionCategoryRequest $request
     * @param \App\Models\InstitutionCategory $institutionCategory
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(InstitutionCategoryRequest $request, InstitutionCategory $institutionCategory)
    {
        $this->authorize('viewAny');
        $institutionCategory->update($request->all());
        return redirect()->route('admin.institutionCategories.index')->with('status', 'institution category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\InstitutionCategory $institutionCategory
     * @return \Illuminate\Http\RedirectResponse|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(InstitutionCategory $institutionCategory)
    {
        $this->authorize('viewAny');
        $institutionCategory->delete();
        return redirect()->route('admin.institutionCategories.index')->with('status', 'institution category deleted!');
    }
}
