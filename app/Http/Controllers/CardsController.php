<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\User;
use Illuminate\Http\Request;

class CardsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $card = new Card();
        $card->user()->associate($request->user());
        $card->institution_id = $request->input('institution_id');
        $card->save();
        return redirect()->route('client.institutions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Card $card
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Card $card)
    {
        $this->authorize('view', $card);
        return view('client.cards.show', compact('card'));
    }
}
