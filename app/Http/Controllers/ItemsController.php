<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $item = new Item();
        $item->card_id = $request->input('card_id');
        $item->dish_id = $request->input('dish_id');
        $item->price = $request->input('price');
        $item->dish_name = $request->input('dish_name');
        $item->qty = $request->input('qty');
        $item->save();

        $request->session()->push('items', $item->id);

        return response()->json(['item' => view('client.items.item', compact('item'))->render()], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Item $item
     * @param Card $card
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Card $card, Item $item, User $user)
    {
        session()->forget(['items', $item->id]);
        $item->delete();
        return response()->json([], 204);
    }
}
