<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DishesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Dish $dish
     * @return Factory|View|Response
     */
    public function show(Dish $dish)
    {
        return view('client.dishes.show', compact('dish'));
    }
}
