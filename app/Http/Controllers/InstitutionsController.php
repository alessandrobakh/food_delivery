<?php

namespace App\Http\Controllers;

use App\Models\DishCategory;
use App\Models\Institution;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InstitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     */
    public function index()
    {
        $institutions = Institution::all();
        return view('client.institutions.index', compact('institutions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Institution  $institution
     * @return Factory|View|Response
     */
    public function show(Institution $institution)
    {
        return view('client.institutions.show', compact('institution'));
    }
}
