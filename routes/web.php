<?php

use App\Http\Controllers\Admin\DishCategoriesController as AdminDishCategoriesController;
use App\Http\Controllers\Admin\DishesController as AdminDishesController;
use App\Http\Controllers\Admin\InstitutionCategoriesController as AdminInstitutionCategoriesController;
use App\Http\Controllers\Admin\InstitutionsController as AdminInstitutionsController;
use App\Http\Controllers\Admin\PagesController as AdminPagesController;
use App\Http\Controllers\CardsController;
use App\Http\Controllers\DishesController;
use App\Http\Controllers\InstitutionsController;
use App\Http\Controllers\ItemsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InstitutionsController::class, 'index'])->name('index');

Route::prefix('admin')->name('admin')->middleware('auth')->resource('admin/dishCategories', AdminDishCategoriesController::class)->except(['show']);
Route::prefix('admin')->name('admin')->middleware('auth')->resource('admin/institutionCategories', AdminInstitutionCategoriesController::class)->except(['show']);
Route::prefix('admin')->name('admin')->middleware('auth')->resource('admin/dishes', AdminDishesController::class);
Route::prefix('admin')->name('admin')->middleware('auth')->resource('admin/institutions', AdminInstitutionsController::class);
Route::prefix('admin')->name('admin')->middleware('auth')->resource('admin/pages', AdminPagesController::class)->only(['index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('client')->name('client')->resource('institutions', InstitutionsController::class)->only(['index', 'show']);
Route::prefix('client')->name('client')->middleware('auth')->resource('cards', CardsController::class)->only(['show', 'store']);
Route::prefix('client')->name('client')->middleware('auth')->resource('cards.items', ItemsController::class)->only(['store', 'destroy']);
Route::prefix('client')->name('client')->resource('dishes', DishesController::class)->only(['show']);

