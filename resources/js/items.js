$(document).ready(function () {
    $('.order-dish-btn').click(function (event) {
        event.preventDefault();

        let element = event.currentTarget;
        const dishId = $(element).data('dish-id');

        const data = $(`.order-dish-form-${dishId}`).serialize();
        const cardId = $(element).data('card-id');

        $.ajax({
            method: "POST",
            url: `/cards/${cardId}/items`,
            data: data
        }).done(function (response) {
            console.log('URA', response);
        }).fail(function (response) {
            console.log('O NET', response)
        });
    });

    $('.delete-item').click(function (event) {
        let element = event.currentTarget;
        const itemId = $(element).data('item-id');
        console.log(itemId);
        const cardId = $('#card_id').val();
        console.log(cardId);
        const token = $(`#csrf-${itemId}`).val();
        console.log(token);

        $.ajax({
            method: "DELETE",
            url: `/cards/${cardId}/items/${itemId}`,
            data: {_token: token}
        }).done(function (response) {
            console.log('udalil', response);
            $(`#item-${itemId}`).remove();
        }).fail(function (response) {
            console.log('ne udalil',response);
        });
    });

    // $('#delete-all-items').click(function (event) {
    //     event.preventDefault();
    //
    //     $('.table').remove();
    //
    //     $('.buy-button').remove();
    //
    //     $('#total-items').remove();
    //
    //     $('#delete-all-items').remove();
    //
    //     $("<p>no orders</p>").appendTo('#no-table');
    // });

});
