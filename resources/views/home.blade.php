@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
            <br>
            <form action="{{route('client.cards.store')}}" method="post">
                @csrf
                <button type="submit" class="btn btn-outline-success">Click to start our service</button>
            </form>
        </div>
    </div>
</div>
@endsection
