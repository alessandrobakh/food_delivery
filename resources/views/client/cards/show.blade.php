@extends('layouts.client')

@section('content')

    <h1>Your orders:</h1>

    @if($card->items->count()>0)

        <input type="hidden" id="card_id" value="{{$card->id}}">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Dish</th>
                <th scope="col">Image</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($card->items as $item)
                <tr class="every-item" id="item-{{$item->id}}">
                    <td>{{$item->dish_name}}</td>
                    <td>
                        <img width="50px" height="50px" src="{{asset('/storage/'. $item->dish->picture)}}" alt="{{asset('/storage/'. $item->dish->picture)}}">
                    </td>
                    <td>{{$item->price}}</td>
                    <td>{{$item->qty}}</td>

                    <td>
                        <span class="delete-item" data-item-id="{{$item->id}}" style="cursor: pointer">
                            <input type="hidden" id="csrf-{{$item->id}}" value="{{csrf_token()}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
</svg>
                        </span>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        <div style="display: none">
            {{$total = 0}}
            @foreach($card->items as $item) {{$total+=$item->price*$item->qty}} @endforeach
        </div>
        <p id="total-items">Total: {{$total}}</p>

        <br>

        <form action="{{route('client.cards.store')}}" method="post">
            @csrf
            <button class="btn btn-primary buy-button" type="submit">Buy</button>
        </form>

    @else
        <p>no orders</p>
    @endif

    <div id="no-table">

    </div>

@endsection
