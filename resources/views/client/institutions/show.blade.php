@extends('layouts.client')

@section('cardcontent')

        <li>
                @if(!is_null(\App\Models\Card::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->where('institution_id', $institution->id)->get()->last()))
                <form action="{{route('client.cards.show', ['card' => \App\Models\Card::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->where('institution_id', $institution->id)->get()->last()])}}" method="post">
                    @csrf
                    @method('get')
                    <button type="submit" class="btn btn-outline-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                            <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                        </svg></button>
                </form>
            @else
                <form action="{{route('client.cards.store')}}" method="post">
                    @csrf

                    <input type="hidden" name="institution_id" value="{{$institution->id}}">

                    <button type="submit" class="btn btn-outline-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                            <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                        </svg></button>
                </form>
            @endif
        </li>

@endsection

@section('content')
    <div class="container">
        <h1>Order food at: {{$institution->name}} | {{$institution->institutionCategory->name}}</h1>

        <p>{{$institution->desc}}</p>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
            @foreach($institution->dishes->sortBy('dish_category_id') as $dish)
                    <div class="col mb-2">
                        <div class="card">
                            <a href="{{route('client.dishes.show', ['dish' => $dish])}}">
                                <img src="{{asset('/storage/' . $dish->picture)}}" class="card-img-top" alt="{{asset('/storage/' . $dish->picture)}}">
                            </a>
                            <div class="card-body">
                                <h5>Category: {{$dish->dishCategory->name}}</h5>
                                <h4 class="card-title">
                                    <a href="{{route('client.dishes.show', ['dish' => $dish])}}">{{$dish->name}}</a>
                                </h4>
                                <h4 class="card-title">{{$dish->price}} som</h4>
                                <form class="order-dish-form-{{$dish->id}}">
                                    @csrf
                                    <input type="hidden" name="card_id" value="{{\App\Models\Card::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->where('institution_id', $institution->id)->get()->last()->id}}">
                                    <input type="hidden" name="dish_id" value="{{$dish->id}}">
                                    <input type="hidden" name="price" value="{{$dish->price}}">
                                    <input type="hidden" name="dish_name" value="{{$dish->name}}">
                                    <div class="form-group row">
                                        <label class="col-4" for="qty-{{$dish->id}}">Quantity:</label>
                                        <input min="0.5" type="number" step=".5" class="form-control col-3" id="qty-{{$dish->id}}" name="qty" value="1">
                                        @error('price')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    @auth()
                                        <button type="submit" class="btn btn-outline-success order-dish-btn" data-dish-id="{{$dish->id}}"
                                                data-card-id="{{ Auth::user()->cards->last()->id }}">Add to Cart</button>
                                    @endauth
                                    <a href="{{ route('client.dishes.show', ['dish' => $dish]) }}" class="btn btn-outline-info">About Dish</a>
                                </form>
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>
@endsection
