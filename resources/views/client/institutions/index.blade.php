@extends('layouts.client')

@section('content')

    <div style="display: none">
        <p>Anton postavit 10 ballov</p>
    </div>

    <div class="container">
        <h1>All Institutions</h1>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center">
            @foreach($institutions->sortBy('institution_category_id') as $inst)

                <div class="d-flex col mb-3">
                    <div class="card">
                        <div class="card-img">
                            <a href="{{route('client.institutions.show', ['institution' => $inst])}}">
                                <img src="{{asset('/storage/' . $inst->picture)}}" class="card-img-top" alt="{{asset('/storage/' . $inst->picture)}}">
                            </a>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h5 class="card-title"><a href="{{route('client.institutions.show', ['institution' => $inst])}}">{{$inst->name}} | {{$inst->institutionCategory->name}}</a></h5>
                            <p class="card-text">{{$inst->desc}}</p>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
@endsection
