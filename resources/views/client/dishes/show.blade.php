@extends('layouts.client')

@section('content')

    <h1>Dish: {{lcfirst($dish->name)}}</h1>

    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{asset('/storage/' . $dish->picture)}}" class="img-fluid rounded-start" alt="{{asset('/storage/' . $dish->picture)}}">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$dish->name}}</h5>
                    <p class="card-text">{{$dish->desc}}</p>
                    <p class="card-text">{{$dish->price}}</p>
                    <p class="card-text">{{$dish->institution->name}}</p>
                    <p class="card-text">{{$dish->dishCategory->name}}</p>
                </div>
            </div>
        </div>
    </div>
    <form class="order-dish-form-{{$dish->id}}">
        @csrf
        <input type="hidden" name="dish_id" value="{{$dish->id}}">
        <input type="hidden" name="price" value="{{$dish->price}}">
        <input type="hidden" name="dish_name" value="{{$dish->name}}">
        <div class="form-group row">
            <label class="col-1" for="qty-{{$dish->id}}">Quantity:</label>
            <input min="0.5" type="number" step=".5" class="form-control col-1" id="qty-{{$dish->id}}" name="qty" value="1">
            @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        @auth()
            <button type="submit" class="btn btn-outline-success order-dish-btn" data-dish-id="{{ $dish->id }}"
                    data-card-id="{{ Auth::user()->cards->last()->id }}">Add to Cart</button>
        @endauth
        <a href="{{ route('client.institutions.show', ['institution' => $dish->institution]) }}"
           class="btn btn-outline-primary">Back</a>
    </form>

@endsection
