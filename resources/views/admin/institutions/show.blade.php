@extends('layouts.admin')

@section('content')

    <h1>{{lcfirst($institution->name)}}</h1>

    <div class="card mb-3" style="max-width: 540px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{asset('/storage/' . $institution->picture)}}" class="img-fluid rounded-start" alt="{{asset('/storage/' . $institution->picture)}}">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$institution->name}}</h5>
                    <p class="card-text">{{$institution->desc}}</p>
                    <p class="card-text">{{$institution->institutionCategory->name}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
