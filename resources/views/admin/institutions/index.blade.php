@extends('layouts.admin')

@section('content')

    <h1>All Institutions</h1>
    <a href="{{route('admin.institutions.create')}}">Create new institution</a>

    @if($institutions->count()>0)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Picture</th>
                <th scope="col">Description</th>
                <th scope="col">Category</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($institutions as $institution)
                <tr>
                    <th scope="row">{{$institution->id}}</th>
                    <td>{{$institution->name}}</td>
                    <td>
                        <img width="50px" height="50px" src="{{asset('/storage/' . $institution->picture)}}" alt="{{asset('/storage/' . $institution->picture)}}">
                    </td>
                    <td>{{$institution->desc}}</td>
                    <td>{{$institution->institutionCategory->name}}</td>
                    <td>
                        <a style="float: left" href="{{route('admin.institutions.edit', ['institution' => $institution])}}"><button type="submit" class="btn btn-outline-warning">Edit</button></a>

                        <a style="float: left" href="{{route('admin.institutions.show', ['institution' => $institution])}}"><button type="submit" class="btn btn-outline-success">Show</button></a>

                        <form style="float: left" method="post" action="{{route('admin.institutions.destroy', ['institution' => $institution])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


    @else
        <p>no institutions</p>
    @endif

@endsection
