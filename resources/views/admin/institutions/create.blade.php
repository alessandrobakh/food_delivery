@extends('layouts.admin')

@section('content')

    <form enctype="multipart/form-data" action="{{route('admin.institutions.store')}}" method="post">
        @csrf

        <div class="form-group">
            <label for="name">name</label>
            <input type="text" class="form-control" id="name" name="name">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="desc">Description</label>
            <textarea class="form-control" id="desc" name="desc"></textarea>
            @error('desc')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <select class="custom-select" name="institution_category_id">
                @foreach($instCategories as $instCat)
                    <option value="{{$instCat->id}}">{{$instCat->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="picture">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            @error('picture')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

@endsection
