@extends('layouts.admin')

@section('content')

    <h1>{{lcfirst($dish->name)}}</h1>

    <div class="card mb-3" style="max-width: 540px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{asset('/storage/' . $dish->picture)}}" class="img-fluid rounded-start" alt="{{asset('/storage/' . $dish->picture)}}">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$dish->name}}</h5>
                    <p class="card-text">{{$dish->desc}}</p>
                    <p class="card-text">{{$dish->price}}</p>
                    <p class="card-text">{{$dish->institution->name}}</p>
                    <p class="card-text">{{$dish->dishCategory->name}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
