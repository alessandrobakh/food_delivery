@extends('layouts.admin')

@section('content')

    <form enctype="multipart/form-data" action="{{route('admin.dishes.update', ['dish' => $dish])}}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="name">name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$dish->name}}">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="number" step=".001" class="form-control" id="price" name="price" value="{{$dish->price}}">
            @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="desc">Description</label>
            <textarea class="form-control" id="desc" name="desc">{{$dish->desc}}</textarea>
            @error('desc')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <select class="custom-select" name="institution_id">
                @foreach($institutions as $institution)
                    <option @if($institution->id == $dish->institution->id) selected @endif value="{{$institution->id}}">{{$institution->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="custom-select" name="dish_category_id">
                @foreach($dishCategories as $dishCat)
                    <option @if($dishCat->id == $dish->dishCategory->id) selected @endif value="{{$dishCat->id}}">{{$dishCat->name}}</option>
                @endforeach
            </select>
        </div>
        <img src="{{asset('/storage/' . $dish->picture)}}" alt="{{$dish->picture}}" style="width:100px;height:100px;"><br/><br>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="picture">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            @error('picture')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

@endsection
