@extends('layouts.admin')

@section('content')

    <h1>All Dishes</h1>
    <a href="{{route('admin.dishes.create')}}">Create new dish</a>

    @if($dishes->count()>0)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Picture</th>
                <th scope="col">Price</th>
                <th scope="col">Description</th>
                <th scope="col">Institution</th>
                <th scope="col">Category</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dishes as $dish)
                <tr>
                    <th scope="row">{{$dish->id}}</th>
                    <td>{{$dish->name}}</td>
                    <td>
                        <img width="50px" height="50px" src="{{asset('/storage/' . $dish->picture)}}" alt="{{asset('/storage/' . $dish->picture)}}">
                    </td>
                    <td>{{$dish->price}}</td>
                    <td>{{$dish->desc}}</td>
                    <td>
                        {{$dish->institution->name}}
                    </td>
                    <td>{{$dish->dishCategory->name}}</td>
                    <td>
                        <a style="float: left" href="{{route('admin.dishes.edit', ['dish' => $dish])}}"><button type="submit" class="btn btn-outline-warning">Edit</button></a>

                        <a style="float: left" href="{{route('admin.dishes.show', ['dish' => $dish])}}"><button type="submit" class="btn btn-outline-success">Show</button></a>

                        <form style="float: left" method="post" action="{{route('admin.dishes.destroy', ['dish' => $dish])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


    @else
    <p>no dishes</p>
    @endif

@endsection
