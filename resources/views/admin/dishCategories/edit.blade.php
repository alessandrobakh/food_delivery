@extends('layouts.admin')

@section('content')

    <form action="{{route('admin.dishCategories.update', ['dishCategory' => $dishCategory])}}" method="post">
        @csrf
        @method('put')

        <label class="col-form-label" for="name">Dish Category Name:</label>
        <input class="form-control col-6" type="text" id="name" name="name" value="{{$dishCategory->name}}">
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>

@endsection
