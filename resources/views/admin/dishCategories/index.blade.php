@extends('layouts.admin')

@section('content')

    <h1>All Dish Categories:</h1>

    <a href="{{route('admin.dishCategories.create')}}">Create new dish category</a>

    <br>
    <br>

    @if($dishCategories->count()>0)

        <ol>
        @foreach($dishCategories as $dishCat)
        <li>
            <h4>
                {{$dishCat->name}}
                <br>
                <a href="{{route('admin.dishCategories.edit', ['dishCategory' => $dishCat])}}">
                    <button class="btn btn-warning">Edit</button> </a>
                <form class="my-1" action="{{route('admin.dishCategories.destroy', ['dishCategory' => $dishCat])}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </h4>
        </li>
        @endforeach
        </ol>
    @else

        <p>no dish categories</p>

    @endif

@endsection
