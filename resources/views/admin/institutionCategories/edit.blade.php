@extends('layouts.admin')

@section('content')

    <form action="{{route('admin.institutionCategories.update', ['institutionCategory' => $institutionCategory])}}" method="post">
        @csrf
        @method('put')

        <label class="col-form-label" for="name">Institution Category Name:</label>
        <input class="form-control col-6" type="text" id="name" name="name" value="{{$institutionCategory->name}}">
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>

@endsection
