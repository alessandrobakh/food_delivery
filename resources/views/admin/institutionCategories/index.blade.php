@extends('layouts.admin')

@section('content')

    <h1>All Institution Categories:</h1>

    <a href="{{route('admin.institutionCategories.create')}}">Create new institution category</a>

    <br>
    <br>

    @if($instCategories->count()>0)

        <ol>
            @foreach($instCategories as $instCat)
                <li>
                    <h4>
                        {{$instCat->name}}
                        <br>
                        <a href="{{route('admin.institutionCategories.edit', ['institutionCategory' => $instCat])}}">
                            <button class="btn btn-warning">Edit</button> </a>
                        <form class="my-1" action="{{route('admin.institutionCategories.destroy', ['institutionCategory' => $instCat])}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </h4>
                </li>
            @endforeach
        </ol>
    @else

        <p>no institution categories</p>

    @endif

@endsection
