@extends('layouts.admin')

@section('content')

    <h1>Hello Admin!</h1>

    <h3>Statistics of food:</h3>

    @if(!is_null(session()->get('items')))
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Institution</th>
                <th scope="col">Dish</th>
            </tr>
            </thead>
            <tbody>
    @foreach(session()->get('items') as $value)
        <tr>
            <td>
                {{\App\Models\Item::find($value)->dish->institution->name}}
            </td>
            <td>
                {{\App\Models\Item::find($value)->dish->name}}
            </td>
        </tr>


    @endforeach
            </tbody>
        </table>
    @endif

@endsection
