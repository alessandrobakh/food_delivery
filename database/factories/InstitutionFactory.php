<?php

namespace Database\Factories;

use App\Models\Institution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class InstitutionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Institution::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstNameMale,
            'picture' => $this->getImage(rand(1,4)),
            'desc' => $this->faker->text(),
            'institution_category_id' => rand(1,4)
        ];
    }

    private function getImage($image_number = 1)
    {
        $path = storage_path() . "/seed_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/'.$image_name, $resize->__toString());
        return 'pictures/'.$image_name;
    }
}
