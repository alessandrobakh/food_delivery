<?php

namespace Database\Factories;

use App\Models\InstitutionCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class InstitutionCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InstitutionCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstNameFemale
        ];
    }
}
