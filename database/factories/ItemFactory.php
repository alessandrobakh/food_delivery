<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'card_id' => rand(1,20),
            'dish_id' => rand(1,10),
            'price' => $this->faker->randomFloat(2,50,500),
            'dish_name' => $this->faker->safeColorName,
            'qty' => $this->faker->randomNumber(2)
        ];
    }
}
