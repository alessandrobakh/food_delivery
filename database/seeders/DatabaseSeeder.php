<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('1234'),
            'isAdmin' => true,
            'phone_number' => '+14432283669',
            'address' => '51888 Ratke Curve Suite 098
                           East Darrionchester, TX 88199'
        ]);

        // \App\Models\User::factory(10)->create();
        $this->call(UserTableSeeder::class);
        $this->call(DishCategoryTableSeeder::class);
        $this->call(InstitutionCategoryTableSeeder::class);
        $this->call(InstitutionTableSeeder::class);
        $this->call(DishTableSeeder::class);
        $this->call(CardTableSeeder::class);
        $this->call(ItemTableSeeder::class);
    }
}
