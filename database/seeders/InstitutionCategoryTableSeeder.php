<?php

namespace Database\Seeders;

use App\Models\InstitutionCategory;
use Illuminate\Database\Seeder;

class InstitutionCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InstitutionCategory::factory()->count(4)->create();
    }
}
