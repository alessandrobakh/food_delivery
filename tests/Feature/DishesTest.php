<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DishesTest extends TestCase
{
    use RefreshDatabase;
    /**
     *@group dishes
     */
    public function test_show_insts()
    {
        $response = $this->get('/dishes/1');
        $response->assertStatus(200);
        $response->assertSeeText('Dish:');
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }
}
