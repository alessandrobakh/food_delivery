<?php

namespace Tests\Feature;

use App\Models\Card;
use App\Models\Dish;
use App\Models\Institution;
use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *@group users
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     *@group users
     */
    public function test_redirect_pages()
    {
        $response = $this->get('admin/pages');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     *@group users
     */
    public function test_redirect_dishes()
    {
        $response = $this->get('admin/dishes');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     *@group users
     */
    public function test_redirect_dish_categories()
    {
        $response = $this->get('admin/dishCategories');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     *@group users
     */
    public function test_redirect_institutions()
    {
        $response = $this->get('admin/institutions');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     *@group users
     */
    public function test_redirect_institution_categories()
    {
        $response = $this->get('admin/institutionCategories');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     *@group users
     */
    public function test_admin_pages()
    {
        $user = User::factory()->create();
        $user->isAdmin = true;
        $this->actingAs($user);
        $response = $this->get('admin/pages');
        $response->assertStatus(200);
        $response = $this->get('admin/institutionCategories');
        $response->assertStatus(200);
        $response = $this->get('admin/institutions');
        $response->assertStatus(200);
        $response = $this->get('admin/dishCategories');
        $response->assertStatus(200);
        $response = $this->get('admin/dishes');
        $response->assertStatus(200);
    }

    /**
     *@group users
     */
    public function test_show_card_another_user()
    {
        $user = User::factory()->has(Card::factory())->create();
        $another_user = User::factory()->create();
        $this->actingAs($another_user);
        $response = $this->get("/cards/{$user->cards->first()->id}");
        $response->assertStatus(403);
    }

    /**
     *@group users
     */
    public function test_user_see_institution_name_and_dish_name()
    {
        $user = User::factory()->create();
        $inst = Institution::factory()->has(Dish::factory())->create();
        $this->actingAs($user);
        $response = $this->get("/institutions/{$inst->id}");
        $response->assertSeeText("$inst->name");
        $response->assertSeeText("{$inst->dishes->first()->name}");
    }

    /**
     *@group balli
     */
    public function test_skolko_ballov_postavit_Anton()
    {
        $response = $this->get('institutions');
        $response->assertSeeText('Anton postavit 10 ballov');
    }
}
