<?php

namespace Tests\Feature;

use App\Models\InstitutionCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InstitutionsTest extends TestCase
{
    use RefreshDatabase;
    /**
     *@group insts
     */
    public function test_index_insts()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSeeText('All Institutions');
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }

    /**
     *@group insts
     */
    public function test_show_insts()
    {
        $response = $this->get('/institutions/1');
        $response->assertStatus(200);
        $response->assertSeeText('Order food at:');
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }
}
